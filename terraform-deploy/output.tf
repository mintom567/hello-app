#outputs the application load balancer hostname
output "alb_hostname" {
  value = aws_alb.main.dns_name
}