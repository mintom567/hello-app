FROM python:3.7.8

RUN mkdir /app

COPY . /app

RUN cd /app && python setup.py install

WORKDIR /app

ENV FLASK_APP=hello

EXPOSE 5000

CMD [ "python", "-m" , "flask", "run", "--host=0.0.0.0", "--port=80"]