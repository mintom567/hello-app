# Hello App

This repo contains source code for Hello-app developed using **Python** and **Terraform** files required for the creation of **AWS Fargate** Service.

## Pipeline
We use `gitlab-CI` for performing pipeline operations with the help of shared-runners provided by gitlab(trial-verison).
Currently gitlab-pipelines won't be visible in github as we are using trial version.

## Stages 

### Test
   * Test stage is for running different test cases for the hello app.
   * Test cases are written with the help of framework called pytest and stored under tests directory.

### Lint:

  * Lint job analyzes the source code and validates the programmatic and stylistic errors in it.

### Publish
   * This stage will dockerize the application and publish the hello app to AWS ECR with the help of `awscli` and `docker-cli` using gitlab pipeline.
   * The image will be published to the AWS ECR registry (`TAG` can be updated in the `gitlab-ci.yml` variables or even you can pass it as a manual variable before triggering the pipeline).

### Terraform-plan
  * Terraform plan will creates an execution plan with preview of changes to build the infrastructure required for deploying the service to AWS Fargate.
  * This job will also update the ECS task definition to make use of the same image which we have published in the previous **publish** stage.

### Terraform-apply
  * Terraform apply will executes the actions proposed in a Terraform plan to create, update, or destroy infrastructure.

## Next Steps

* Integration of AWS App Mesh in AWS Fargate can help in monitor, control, and debug the communications between services for multiple microservice containers management.
    - Traffic Routing and Envoy Proxy services
    - Service-to-Service Authentication(mTLS)
    - TLS enablement
    - Multiple Deployment strategies
    - Improves Security
* AWS Auto Scaling
    - AWS Auto scaling will help in scaling the app smartly based on the requirement and can maintain the performance.
* WAF
    - We can make use of AWS WAF for protecting the app against bots and exploits that consume resources. 
* Log Monitoring
    - With cloud watch we can configure your container instances to send log information to CloudWatch Logs.
* We can also plan to include other jobs for SAST, image-scan, vulnerability-scan, terraform destroy(destroying the infrastructure) etc in the `gitlab-ci.yml`
